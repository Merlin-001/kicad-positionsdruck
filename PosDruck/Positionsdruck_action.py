##############################################################################
# @file    KiCad-Positionsdruck_action.py
# @author  Marvin Kureliuk
# @brief   Change the visibility of silkscreen in Kicad V6 components
# @note    uses Layer User9
##############################################################################
# @licence GNU GPLv3 http://www.gnu.org
#
###############################################################################


import sys
import os
import time

try:
	import wx
except ModuleNotFoundError:
	print("Module wx nicht gefunden. Ende")
# sys.exit(-1)
try:
	from pcbnew import *
except ModuleNotFoundError:
	print("Module pcbnew  nicht gefunden. Ende")


import math
import os

# sys.exit(-1)


class mylogger(object):

	def __init__( self ):


		self.writefileloc = os.path.join( os.path.dirname(os.path.dirname(__file__)),"Log-Positionsdruck.log")
		self.LogONClass = {}
		self.LogONClass["FootprintMkk"] = False
		self.LogONClass["PosdruckPrg"] = False
		self.LogONClass["multiCheckboxRow"] = False
		self.LogONClass["mainPosdruckWindow"] = True
		self.LogONClass["Positionsdruck"] = True

	def startNewFile( self ):

		self.writefile = open(self.writefileloc, "w")
		self.writefile.close()

	def writeline( self, cClass, cFunction, cBlocK, cType, writeLineToLog ):

		try:
			if (not self.LogONClass[cClass]):
				return
		except KeyError:
			return

		textLine = "{} :".format(time.strftime("%Y%m%d_%H%M%S", time.gmtime()))
		textLine += " {}:{}:{} :: {} :: {}".format(cClass, cFunction, cBlocK, cType, writeLineToLog)
		textLine += "\n"

		self.writefile = open(self.writefileloc, "a+")

		self.writefile.write(str(textLine))

		self.writefile.close()

	def close( self ):
		self.writefile.close()

	def DEF_INFO( self ):

		return "INFO"

	def DEF_ERR( self ):

		return "ERROR"

	def DEF_NOTE( self ):

		return "NOTE"

class FootprintMkk(object):

	def __init__( self, logger ):

		self.myLogFile = logger

		#########################
		# Variablen
		#########################
		self.RefName = ""  # z.B. R1 C17 etc...
		self.ItemOnLayer = 0  # ID des Layers auf dem das Bauteil ist. (Top/Bottom)
		self.KicadFootPrintObj = None  # Das Objekt in Kicad
		self.GuiButtonID = None

		self.allUsedLayers = []
		self.allUsedLayersDict = {}

		#########################
		# Variablen Sichbarkeit
		#########################
		# Ist-Zustand
		self.visibleRefSilk = None  # True
		self.hiddenRefSilk = None  # True
		
		self.hiddenFabVal = None  # True
		self.visibleFabVal = None  # True

		self.hasHiddenOutSilk = None  # True
		self.hasFullyHiddenOutSilk = None  # False

		self.hasHiddenSilk = None  # True
		self.hasFullyHiddenSilk = None  # False

		# Soll-Zustand
		self.visibleSilkRefGoal = None  # False
		self.visibleSilkOutGoal = None  # False
		self.visibleFabVisGoal = None  # False

		#########################
		# Lookup-Tabellen
		#########################
		self.__layerNameIdLookup = {
			# Unvollständige Lsite
			"F.Cu":0,
			"In1.Cu":1,
			"In2.Cu":2,
			"In3.Cu":3,
			"In4.Cu":4,
			"In5.Cu":5,
			"In6.Cu":6,
			"In7.Cu":7,
			"In8.Cu":8,
			"In9.Cu":9,
			# hier kommen dann die Innenlagen
			"B.Cu":31,
			# Achtung in den Tech-Lagen folgt erst Bottom dann Front.
			"B.Adhes":32,
			"F.Adhes":33,
			"B.Paste":34,
			"F.Paste":35,
			"B.SilkS":36,
			"F.SilkS":37,
			"B.Mask":38,
			"F.Mask":39,
			"Dwgs.User":40,
			"Cmts.User":41,
			"Eco1.User":42,
			"Eco2.User":43,
			"Edge.Cuts":44,
			"Margin":45,
			"B.CrtYd":46,
			"F.CrtYd":47,
			"B.Fab":48,
			"F.Fab":49,
			"User.1":50,
			"User.2":51,
			"User.3":52,
			"User.4":53,
			"User.5":54,
			"User.6":55,
			"User.7":56,
			"User.8":57,
			"User.9":58,
		}

	def __str__( self ):

		printstr = "[mhkFp:: "
		printstr += "REF: "+str(self.RefName)
		printstr += ";\n    hiddenRefSilk: "+str(self.hiddenRefSilk)
		printstr += ";\n    visibleRefSilk: "+str(self.visibleRefSilk)
		printstr += ";\n    visibleSilkRefGoal: "+str(self.visibleSilkRefGoal)
		printstr += ";\n    hasHiddenSilk: "+str(self.hasHiddenSilk)
		printstr += ";\n    hasFullyHiddenSilk: "+str(self.hasFullyHiddenSilk)
		printstr += ";\n    visibleSilkOutGoal: "+str(self.visibleSilkOutGoal)
		printstr += ";\n    visibleFabVisGoal: "+str(self.visibleFabVisGoal)
		printstr += ";\n    GuiButtonID: "+str(self.GuiButtonID)
		printstr += "]"

		return printstr

	def __repr__( self ):

		printstr = "[mhkFp:: "
		printstr += "REF: "+str(self.RefName)
		printstr += "]"

		return printstr

	def refreshVars( self ):

		self.myLogFile.writeline("FootprintMkk", "refreshVars", "-", self.myLogFile.DEF_INFO(), "start")

		self.__allUsedLayers()  # Welche Layer werden den genutzt

		self.__hasHiddenSilk()
		self.__hasFullyHiddenSilk()

		self.__hasHiddenOutSilk()
		self.__hasFullyHiddenOutSilk()

		self.__visibleRefSilk()
		self.__hiddenRefSilk()

		self.__visibleFabVal()
		self.__hiddenFabVal()

	def setKiCadFootPrintObj( self, KiCadObj ):

		self.KicadFootPrintObj = KiCadObj

		self.RefName = self.KicadFootPrintObj.GetReference()
		self.ItemOnLayer = self.KicadFootPrintObj.GetLayer()

		self.allUsedLayers = []

		# init#
		self.refreshVars()

	def __addLayerlist( self, item ):

		itemLayer = item.GetLayerName()
		itemLayerID = item.GetLayer()

		self.allUsedLayersDict[itemLayer] = itemLayerID

		if itemLayerID not in self.allUsedLayers:
			self.allUsedLayers.append(itemLayerID)

	def __allUsedLayers( self ):

		self.allUsedLayers = []
		self.allUsedLayersDict = {}

		for graphitem in self.KicadFootPrintObj.GraphicalItems():
			self.__addLayerlist(graphitem)

		item = self.KicadFootPrintObj.Reference()
		self.__addLayerlist(item)
		item = self.KicadFootPrintObj.Value()
		self.__addLayerlist(item)
		item = self.KicadFootPrintObj
		self.__addLayerlist(item)

	def __visibleRefSilk( self ):

		self.visibleRefSilk = self.KicadFootPrintObj.Reference().IsVisible()

	def __visibleFabVal( self ):
	
		self.visibleFabVal = self.KicadFootPrintObj.Value().IsVisible()

	def __hiddenRefSilk( self, refreshLayers = False ):

		if (refreshLayers == True):
			self.__allUsedLayers()

		for cmpId in self.__convertLayNamesToID("User.9"):
			if (self.KicadFootPrintObj.Reference().GetLayer() == cmpId):
				self.hiddenRefSilk = True
			else:
				self.hiddenRefSilk = False
				
	def __hiddenFabVal( self, refreshLayers = False ):

		if (refreshLayers == True):
			self.__allUsedLayers()

		for cmpId in self.__convertLayNamesToID("User.9"):
			if (self.KicadFootPrintObj.Value().GetLayer() == cmpId):
				self.hiddenFabVal = True
			else:
				self.hiddenFabVal = False

	def __hasHiddenOutSilk( self, refreshLayers = False ):

		if (refreshLayers == True):
			self.__allUsedLayers()

		for cmpItem in self.getGraphicItemsLayer("User.9"):
			if (cmpItem):
				self.hasHiddenOutSilk = True
				return True

		self.hasHiddenOutSilk = False
		return False

	def __hasFullyHiddenOutSilk( self, refreshLayers = False ):

		if (refreshLayers == True):
			self.__allUsedLayers()

		for cmpItem in self.getGraphicItemsLayer(["F.SilkS", "B.SilkS"]):
			if (cmpItem):
				self.hasFullyHiddenOutSilk = False
				return False

		self.hasFullyHiddenOutSilk = True
		return True

	def __hasHiddenSilk( self, refreshLayers = False ):

		if (refreshLayers == True):
			self.__allUsedLayers()

		for cmpId in self.__convertLayNamesToID("User.9"):
			for layerid in self.allUsedLayers:
				# print("__hasHiddenSilk:layerid {}".format(layerid))
				if (layerid == cmpId):
					self.hasHiddenSilk = True
					return True

		self.hasHiddenSilk = False
		return False

	def __hasFullyHiddenSilk( self, refreshLayers = False ):

		if (refreshLayers == True):
			self.__allUsedLayers()

		for cmpId in self.__convertLayNamesToID(["F.SilkS", "B.SilkS"]):
			for layerid in self.allUsedLayers:
				if (layerid == cmpId):
					self.hasFullyHiddenSilk = False
					return False

		self.hasFullyHiddenSilk = True
		return True

	def setFootprintOutlineVisibility( self, OutlineVisibility ):

		if (isinstance(OutlineVisibility, bool)):
			self.visibleSilkOutGoal = OutlineVisibility
			self.myLogFile.writeline("FootprintMkk", "setFootprintOutlineVisibility", "-", self.myLogFile.DEF_INFO(),
									 "OutlineVisibility :{} with id {} and GUIID {}".format(OutlineVisibility, id(self),
																							self.GuiButtonID))
		else:
			raise TypeError("OutlineVisibility expect Type bool but given Type {}".format(type(OutlineVisibility)))

	def setFootprintReferneceVisibility( self, ReferneceVisibility ):

		if (isinstance(ReferneceVisibility, bool)):
			self.visibleSilkRefGoal = ReferneceVisibility
			self.myLogFile.writeline("FootprintMkk", "setFootprintReferneceVisibility", "-", self.myLogFile.DEF_INFO(),
									 "ReferneceVisibility :{} with id {} and GUIID {}".format(ReferneceVisibility,
																							  id(self),
																							  self.GuiButtonID))
		else:
			raise TypeError("ReferneceVisibility expect Type bool but given Type {}".format(type(ReferneceVisibility)))

	def setFootprintFabValVisibility( self, FabValVisibility ):

		if (isinstance(FabValVisibility, bool)):
			self.visibleFabVisGoal = FabValVisibility
			self.myLogFile.writeline("FootprintMkk", "setFootprintReferneceVisibility", "-", self.myLogFile.DEF_INFO(),
									 "FabValVisibility :{} with id {} and GUIID {}".format(FabValVisibility,
																							  id(self),
																							  self.GuiButtonID))
		else:
			raise TypeError("FabValVisibility expect Type bool but given Type {}".format(type(FabValVisibility)))

	def setGuiButtonID( self, ButtonId ):

		self.myLogFile.writeline("FootprintMkk", "setGuiButtonID", "-", self.myLogFile.DEF_INFO(),
								 "BID :{}".format(ButtonId))

		if (isinstance(ButtonId, int)):
			self.GuiButtonID = ButtonId
		else:
			self.GuiButtonID = None

	def getButtonId( self ):

		return self.GuiButtonID

	def getRefName( self ):

		return self.RefName

	def getRefVis( self ):

		if (self.visibleSilkRefGoal == None):
			return not self.hiddenRefSilk
		else:
			return self.visibleSilkRefGoal

	def getOutVis( self ):

		if (self.visibleSilkOutGoal == None):

			return not self.hasFullyHiddenOutSilk

		else:
			return self.visibleSilkOutGoal

	def getFabVis( self ):

		if (self.visibleFabVisGoal == None):
		
			return not self.hiddenFabVal

		else:
			return self.visibleFabVisGoal
			
	def getGraphicItems( self ):

		gi = self.KicadFootPrintObj.GraphicalItems()

		if (len(gi) == 0):
			return []

		return self.KicadFootPrintObj.GraphicalItems()

	def __convertLayNamesToID( self, Layerlist ):

		if (isinstance(Layerlist, str)):
			LayerIdList = [int(self.__layerNameIdLookup[Layerlist])]
			return LayerIdList
		if (isinstance(Layerlist, int)):
			LayerIdList = [int(Layerlist)]
			return LayerIdList

		if (isinstance(Layerlist, list)):
			LayerIdList = []
			for item in Layerlist:
				LayerIdList.extend(self.__convertLayNamesToID(item))
			return LayerIdList

	def getGraphicItemsLayer( self, Layerlist ):

		LayerIdList = self.__convertLayNamesToID(Layerlist)

		if (isinstance(LayerIdList, list)):

			# print("Num items {}; Layerlist {}".format(len(self.KicadFootPrintObj.GraphicalItems()),LayerIdList))

			for graphitem in self.KicadFootPrintObj.GraphicalItems():
				itemLayer = graphitem.GetLayerName()
				itemLayerID = graphitem.GetLayer()

				for cmpID in LayerIdList:
					if (itemLayerID == cmpID):
						# print("hit: {}-ID{} vs {}".format(itemLayer,itemLayerID,cmpID))
						yield graphitem, cmpID

	def getTextLayer( self, Layerlist ):

		LayerIdList = self.__convertLayNamesToID(Layerlist)

		if (isinstance(LayerIdList, list)):

			textItemList = self.KicadFootPrintObj.Reference()

			if (not isinstance(textItemList, list)):
				textItemList = [textItemList]

			for textItem in textItemList:
				itemLayer = textItem.GetLayerName()
				itemLayerID = textItem.GetLayer()

				for cmpID in LayerIdList:
					if (itemLayerID == cmpID):
						yield textItem, cmpID

	def doModifikcations(self):

		self.myLogFile.writeline("FootprintMkk", "doModifikcations", "-", self.myLogFile.DEF_INFO(),
								 "Start")

		InvisLayer = self.__layerNameIdLookup["User.9"]



		# Fab-Value ein-/ausblenden
		self.myLogFile.writeline("FootprintMkk", "doModifikcations", "-", self.myLogFile.DEF_INFO(),
								 "Start-FAB-Layer")
		# Auf welchen Layer ist das Bauteil ? [Top/Bot]
		if ((self.ItemOnLayer == self.__layerNameIdLookup["F.Cu"])):
			VisLayer = self.__layerNameIdLookup["F.Fab"]
		elif ((self.ItemOnLayer == self.__layerNameIdLookup["B.Cu"])):
			VisLayer = self.__layerNameIdLookup["B.Fab"]
		else:
			self.myLogFile.writeline("FootprintMkk", "doItemVisibilityChange", "-", self.myLogFile.DEF_ERR(),
									 "ItemOnLayer Layer hat einen unzulässigen wert : {}".format(self.ItemOnLayer))
			raise ValueError("ItemOnLayer Layer hat einen unzulässigen wert : {}".format(self.ItemOnLayer))

		# blenden
		if (self.getFabVis()):
			self.KicadFootPrintObj.Value().SetLayer(VisLayer)
		else:
			self.KicadFootPrintObj.Value().SetLayer(InvisLayer)



		# Ref-Value ein-/ausblenden (Silk)
		self.myLogFile.writeline("FootprintMkk", "doModifikcations", "-", self.myLogFile.DEF_INFO(),
								 "Start-REF-Layer")
		# Auf welchen Layer ist das Bauteil ? [Top/Bot]
		if ((self.ItemOnLayer == self.__layerNameIdLookup["F.Cu"])):
			VisLayer = self.__layerNameIdLookup["F.SilkS"]
		elif ((self.ItemOnLayer == self.__layerNameIdLookup["B.Cu"])):
			VisLayer = self.__layerNameIdLookup["B.SilkS"]
		else:
			self.myLogFile.writeline("FootprintMkk", "doItemVisibilityChange", "-", self.myLogFile.DEF_ERR(),
									 "ItemOnLayer Layer hat einen unzulässigen wert : {}".format(self.ItemOnLayer))
			raise ValueError("ItemOnLayer Layer hat einen unzulässigen wert : {}".format(self.ItemOnLayer))
		# blenden
		if (self.getRefVis()):
			self.KicadFootPrintObj.Reference().SetLayer(VisLayer)
		else:
			self.KicadFootPrintObj.Reference().SetLayer(InvisLayer)


		# Graph-Items ein-/ausblenden
		self.myLogFile.writeline("FootprintMkk", "doModifikcations", "-", self.myLogFile.DEF_INFO(),
								 "Start-Graph-Items-Layer")
		# Auf welchen Layer ist das Bauteil ? [Top/Bot]
		# ist noch von der REF-Prüfung richtig gesetzt.
		# blenden

		if (self.getOutVis()):
			self.myLogFile.writeline("FootprintMkk", "doModifikcations", "-", self.myLogFile.DEF_INFO(),
									 "getOutVis true")
			for GraphItem, cmpID in self.getGraphicItemsLayer(["F.SilkS", "B.SilkS", "User.9"]):
				self.myLogFile.writeline("FootprintMkk", "doModifikcations", "-", self.myLogFile.DEF_INFO(),
										 "GraphItem {0}".format(cmpID))
				GraphItem.SetLayer(VisLayer)
		else:
			self.myLogFile.writeline("FootprintMkk", "doModifikcations", "-", self.myLogFile.DEF_INFO(),
									 "getOutVis true")
			for GraphItem, cmpID in self.getGraphicItemsLayer(["F.SilkS", "B.SilkS", "User.9"]):
				self.myLogFile.writeline("FootprintMkk", "doModifikcations", "-", self.myLogFile.DEF_INFO(),
										 "GraphItem {0}".format(cmpID))
				GraphItem.SetLayer(InvisLayer)

		self.myLogFile.writeline("FootprintMkk", "doModifikcations", "-", self.myLogFile.DEF_INFO(),
								 "Ende")



class PosdruckPrg(object):

	def __init__( self, logger ):

		self.myLogFile = logger
		
		self.myLogFile.writeline("PosdruckPrg", "__init__", "-", self.myLogFile.DEF_INFO(), "start")

		self.myFpList = []

		self.pcb = GetBoard()
		self.pcbFootprints = self.pcb.Footprints()
		
		self.myLogFile.writeline("PosdruckPrg", "__init__", "-", self.myLogFile.DEF_INFO(), "Ende")

	def listAllItems( self ):

		self.myFpList = []
		self.myFpDict = {}
		locFpDict = {}

		self.pcbFootprints = self.pcb.Footprints()
		for fpi in self.pcbFootprints:
			newFP = FootprintMkk(self.myLogFile)
			newFP.setKiCadFootPrintObj(fpi)
			refname = newFP.getRefName()
			self.myLogFile.writeline("FootprintMkk", "listAllItems", "-", self.myLogFile.DEF_INFO(),
									 "New fpItem with refname {} with id {}".format(refname, id(newFP)))

			locFpDict[str(refname)] = newFP

		# Sortierte Liste erstellen

		self.myFpDict = dict(sorted(locFpDict.items()))

		for key in self.myFpDict:
			self.myFpList.append(self.myFpDict[key])

	def setFpItem( self, label, guiId, refValue, outValue ):

		for fpItem in self.myFpList:

			if (isinstance(fpItem, FootprintMkk)):
				buttonID = fpItem.getButtonId()
				if (buttonID == guiId):

					self.myLogFile.writeline("FootprintMkk", "setFpItem", "-", self.myLogFile.DEF_INFO(),
											 "Chaning fpItem buttonID {} with id {}".format(buttonID, id(fpItem)))

					fpItem.setFootprintReferneceVisibility(refValue)
					fpItem.setFootprintOutlineVisibility(outValue)
					return True
				else:
					self.myLogFile.writeline("FootprintMkk", "setFpItem", "-", self.myLogFile.DEF_ERR(),
											 "MISSMATCHED guiId {} vs getButtonId {}".format(guiId, buttonID))
			else:
				self.myLogFile.writeline("FootprintMkk", "setFpItem", "-", self.myLogFile.DEF_ERR(),
										 "MISSMATCHED type {} vs getButtonId {}".format(type(fpItem), buttonID))

		return False

	def getFPList( self ):

		return self.myFpList

	def getFPDict( self ):

		return self.myFpDict

	def getFPLen( self ):

		return len(self.myFpList)

	def RunModifyFootprints( self ):

		self.myLogFile.writeline("PosdruckPrg", "RunModifyFootprints", "-", self.myLogFile.DEF_INFO(), "start")

		for fpItem in self.myFpList:
			if (isinstance(fpItem, FootprintMkk)):
				self.myLogFile.writeline("PosdruckPrg", "RunModifyFootprints", "-", self.myLogFile.DEF_INFO(),
										 "modifyFootprintMkkItem")
				fpItem.doModifikcations()
			else:
				print("Warning: item has erong type. Skipping type {}".format(type(fpItem)))
				self.myLogFile.writeline("PosdruckPrg", "RunModifyFootprints", "-", self.myLogFile.DEF_ERR(),
										 "Warning: item has erong type. Skipping type {}".format(type(fpItem)))

class multiCheckboxRow(object):

	def __init__( self, logger, parentPanel, checkboxID ):
		super(multiCheckboxRow, self).__init__()

		self.myLogFile = logger

		self.checkboxID = checkboxID

		self.checkboxReference = wx.CheckBox(parentPanel, wx.ID_ANY)
		self.checkboxOutline = wx.CheckBox(parentPanel, wx.ID_ANY)
		self.checkboxFabVal = wx.CheckBox(parentPanel, wx.ID_ANY)
		self.checkboxText = wx.StaticText(parentPanel, label = str("This item is not set yet."))

	def getRefValue( self ):

		return self.checkboxReference.GetValue()

	def getOutValue( self ):

		return self.checkboxOutline.GetValue()
		
	def getFabValue( self ):

		return self.checkboxFabVal.GetValue()

	def getRefName( self ):

		return self.checkboxText.GetLabel()

	def setBoxValues( self, Ref, Outline, FabVal ):
		# def setBoxValues( self, Ref, Outline, Refname = None):

		# if(isinstance(Refname, str)):
		#	self.checkboxOutline.SetValue(Refname)

		if (isinstance(Ref, bool)):
			self.checkboxReference.SetValue(Ref)

		if (isinstance(Outline, bool)):
			self.checkboxOutline.SetValue(Outline)
			
		if (isinstance(FabVal, bool)):
			self.checkboxFabVal.SetValue(FabVal)

	def getWxCheckboxRefObj( self ):

		return self.checkboxReference

	def getWxChekboxOutlineObj( self ):

		return self.checkboxOutline
		
	def getWxChekboxFabValObj( self ):

		return self.checkboxFabVal

	def getWxTextObj( self ):

		return self.checkboxText

	def getCheckboxID( self ):

		return self.checkboxID

	def setVisible( self ):

		self.checkboxReference.Show()
		self.checkboxOutline.Show()
		self.checkboxFabVal.Show()
		self.checkboxText.Show()

	def setInvisible( self ):

		self.checkboxReference.Hide()
		self.checkboxOutline.Hide()
		self.checkboxFabVal.Hide()
		self.checkboxText.SetLabel("")

	def setRefName( self, Refname ):

		self.checkboxText.SetLabel(Refname)


class mainPosdruckWindow(wx.Frame):

	def __init__( self, logger, *args, **kwargs ):

		super(mainPosdruckWindow, self).__init__(*args, **kwargs)

		self.myLogFile = logger

		self.myLogFile.writeline("mainPosdruckWindow", "__init__", "-", self.myLogFile.DEF_INFO(), "start")

		#########################
		# Variablen
		#########################

		self.NumCheckboxColumns = 2
		self.NumCheckboxRows = 10

		self.MaxNumCheckboxperPage = self.NumCheckboxRows*self.NumCheckboxColumns

		self.CheckboxOnLastPage = 0
		self.AktiveCheckboxOnPage = 0

		self.AktuelleSeite = 1
		self.AnzahlSeiten = 1

		self.NumBauteil = 0

		self.FPonVisPageList = []
		self.FPonVisPageDict = {}

		#########################
		# GUI
		#########################

		self.MidPanMultiCheckboxList = []
		self.MidPanCheckPanList = []

		# Liste Objekte die in den initFunbktionen erstellt werden
		# self.menubar
		# self.fileMenu
		# self.fileItem
		# self.upperPanelHBox
		# self.upperPanelSTxt1
		# self.midPanVBox
		# self.midPanHBox
		# self.MidPanCheckPanList = []
		# self.MidPanMultiCheckboxList = []
		# self.getCheckboxID = 0
		# self.ButtonAllRefOn
		# self.ButtonAllRefOff
		# self.ButtonAllOutlineOn
		# self.ButtonAllOutlineOff
		# self.ButtonAllOn
		# self.ButtonAllOff
		# self.ButtonNext
		# self.ButtonPrev
		# self.lowerPanHBox
		# self.lowerPanButtonOK
		# self.lowerPanButtonCancel
		# self.lowerPanHBox
		# self.fontNormalText
		# self.MainPanel
		# self.MainPanelVBox
		# self.upperPanel
		# self.midPan
		# self.lowerPanel

		self.myLogFile.writeline("mainPosdruckWindow", "__init__", "GUI", self.myLogFile.DEF_INFO(), "start")
		self.__initGuiMainWin()

		#########################
		# Pos-druck
		#########################
		self.myLogFile.writeline("mainPosdruckWindow", "__init__", "Posdruck", self.myLogFile.DEF_INFO(), "start")
		
		self.Posdruck = PosdruckPrg(self.myLogFile)

		# Liste Objekte die in den initFunbktionen erstellt werden

		# Intital alle Daten laden
		
		self.__PosdruckBasicSetUp()

		self.myLogFile.writeline("mainPosdruckWindow", "__init__", "Posdruck", self.myLogFile.DEF_INFO(), "Ende")

		self.myLogFile.writeline("mainPosdruckWindow", "__init__", "-", self.myLogFile.DEF_INFO(), "done")

	def __PosdruckBasicSetUp(self):

		# Alle Bauteile erfassen
		self.Posdruck.listAllItems()

		self.NumBauteil = self.Posdruck.getFPLen()
		self.myLogFile.writeline("mainPosdruckWindow", "__PosdruckBasicSetUp", "-", self.myLogFile.DEF_INFO(),
								 "NumBauteil: {}".format(self.NumBauteil))

		self.calcNeedePageConfig()
		self.updateSeitenzahlen()
		self.setAktivCheckbox()

	def calcNeedePageConfig( self ):

		self.myLogFile.writeline("mainPosdruckWindow", "calcNeedePageConfig", "-", self.myLogFile.DEF_INFO(), "Start")

		self.AnzahlSeiten = math.ceil(self.NumBauteil/self.MaxNumCheckboxperPage)
		self.myLogFile.writeline("mainPosdruckWindow", "calcNeedePageConfig", "-", self.myLogFile.DEF_INFO(),
								 "AnzahlSeiten: {}".format(self.AnzahlSeiten))
		self.CheckboxOnLastPage = self.NumBauteil%self.MaxNumCheckboxperPage
		self.myLogFile.writeline("mainPosdruckWindow", "calcNeedePageConfig", "-", self.myLogFile.DEF_INFO(),
								 "CheckboxOnLastPage: {}".format(self.CheckboxOnLastPage))

	def clearButtonIDConnection( self ):

		for fpitem in self.FPonVisPageList:

			if (isinstance(fpitem, FootprintMkk)):
				fpitem.setGuiButtonID(None)

	def setAktivCheckbox( self ):

		self.myLogFile.writeline("mainPosdruckWindow", "setAktivCheckbox", "-", self.myLogFile.DEF_INFO(),
								 "Start")

		self.FPonVisPageList = []
		self.FPonVisPageDict = {}

		aktivNumCheckboxen = self.MaxNumCheckboxperPage
		fpListe = self.Posdruck.getFPList()

		fplOffset = self.MaxNumCheckboxperPage*(self.AktuelleSeite-1)

		if ((len(fpListe) == 0) or (self.AktuelleSeite == self.AnzahlSeiten)):
			aktivNumCheckboxen = self.CheckboxOnLastPage

		self.myLogFile.writeline("mainPosdruckWindow", "setAktivCheckbox", "-", self.myLogFile.DEF_INFO(),
								 "aktivNumCheckboxen : {}".format(aktivNumCheckboxen))

		for CheckboxNum, chekbox in enumerate(self.MidPanMultiCheckboxList):

			if (isinstance(chekbox, multiCheckboxRow)):
				if (CheckboxNum < aktivNumCheckboxen):
					chekbox.setVisible()
					self.myLogFile.writeline("mainPosdruckWindow", "setAktivCheckbox", "-", self.myLogFile.DEF_INFO(),
											 "setVisible: {}".format(CheckboxNum))

					fpItem = fpListe[fplOffset+CheckboxNum]
					if (isinstance(fpItem, FootprintMkk)):
						label = fpItem.getRefName()
						refValue = fpItem.getRefVis()
						outValue = fpItem.getOutVis()
						FabVal = fpItem.getFabVis()
						fpItem.setGuiButtonID(CheckboxNum)
						self.FPonVisPageList.append(fpItem)
						self.FPonVisPageDict[str(label)] = fpItem
						self.myLogFile.writeline("mainPosdruckWindow", "setAktivCheckbox", "-",
												 self.myLogFile.DEF_INFO(),
												 "label: {}; refValue: {}; outValue: {}; FabVal: {}; CheckboxNum: {};".format(label,
																												  refValue,
																												  outValue,
																												  FabVal,
																												  CheckboxNum))
					else:
						label = "Data Error"
						refValue = False
						outValue = False
						FabVal = False
						self.myLogFile.writeline("mainPosdruckWindow", "setAktivCheckbox", "-",
												 self.myLogFile.DEF_ERR(),
												 "Data Error fpItem wrong type: {}".format(type(fpItem)))

					chekbox.setRefName(str(label))
					chekbox.setBoxValues(refValue, outValue, FabVal)


				else:
					chekbox.setInvisible()
					self.myLogFile.writeline("mainPosdruckWindow", "setAktivCheckbox", "-", self.myLogFile.DEF_INFO(),
											 "setInvisible : {}".format(CheckboxNum))

		self.myLogFile.writeline("mainPosdruckWindow", "setAktivCheckbox", "-", self.myLogFile.DEF_INFO(),
								 "End")

	def updateTextTopline( self, newText ):

		self.myLogFile.writeline("mainPosdruckWindow", "updateTextTopline", "-", self.myLogFile.DEF_INFO(),
								 "{}".format(newText))

		self.upperPanelSTxt1.SetLabel("{}".format(newText))

	def ToplineStdText( self ):

		self.updateTextTopline("Bitte Auswahl treffen...")

	def updateSeitenzahlen( self ):

		self.myLogFile.writeline("mainPosdruckWindow", "updateSeitenzahlen", "-", self.myLogFile.DEF_INFO(), "Start")

		self.StaticTextAktuelleSeite.SetLabel("{} von {}".format(self.AktuelleSeite, self.AnzahlSeiten))

	def storeCheckboxValue( self ):

		self.myLogFile.writeline("mainPosdruckWindow", "storeCheckboxValue", "-", self.myLogFile.DEF_INFO(), "Start")

		locVisFpDict = self.FPonVisPageDict.copy()

		for CheckboxNum, checkbox in enumerate(self.MidPanMultiCheckboxList):
			if (isinstance(checkbox, multiCheckboxRow)):

				refValue = checkbox.getRefValue()
				outValue = checkbox.getOutValue()
				FabVal = checkbox.getFabValue()
				guiId = checkbox.getCheckboxID()
				label = checkbox.getRefName()
				if (label != ""):
					self.myLogFile.writeline("mainPosdruckWindow", "storeCheckboxValue", "-", self.myLogFile.DEF_INFO(),
											 "refValue {}; outValue {}; FabVal {}; guiId {}; label {}".format(refValue, outValue,FabVal,
																								   guiId, label))
					# self.Posdruck.setFpItem(label,guiId,refValue,outValue)
					fpItem = locVisFpDict[label]
					fpItem.setFootprintReferneceVisibility(refValue)
					fpItem.setFootprintOutlineVisibility(outValue)
					fpItem.setFootprintFabValVisibility(FabVal)
					locVisFpDict.pop(label)#
					
		self.myLogFile.writeline("mainPosdruckWindow", "storeCheckboxValue", "-", self.myLogFile.DEF_INFO(), "ENDE")

	def switchPage( self, nextpage = None ):

		self.myLogFile.writeline("mainPosdruckWindow", "switchPage", "-", self.myLogFile.DEF_INFO(), "start")

		if (nextpage == None):
			nextpage = self.AktuelleSeite

		if ((nextpage <= self.AnzahlSeiten) and (nextpage >= 1)):
			self.myLogFile.writeline("mainPosdruckWindow", "switchPage", "-", self.myLogFile.DEF_INFO(), "check ok")
			# self.storeCheckboxValue() # Muss vcn der Calling Fn gerufenwerden !!!
			self.AktuelleSeite = nextpage
			self.updateSeitenzahlen()
			self.clearButtonIDConnection()
			self.setAktivCheckbox()

		self.myLogFile.writeline("mainPosdruckWindow", "switchPage", "-", self.myLogFile.DEF_INFO(), "ende")

	def updatePage( self ):

		self.myLogFile.writeline("mainPosdruckWindow", "updatePage", "-", self.myLogFile.DEF_INFO(), "start")

		for fpItem in self.FPonVisPageList:

			if (isinstance(fpItem, FootprintMkk)):
				label = fpItem.getRefName()
				refValue = fpItem.getRefVis() 
				outValue = fpItem.getOutVis()
				FabVal = fpItem.getFabVis()
				guiId = fpItem.getButtonId()
				self.MidPanMultiCheckboxList[guiId].setBoxValues(refValue, outValue, FabVal)

				self.myLogFile.writeline("mainPosdruckWindow", "updatePage", "-",
										 self.myLogFile.DEF_INFO(),
										 "label: {}; refValue: {}; outValue: {}; FabVal: {}; CheckboxNum: {}; with ID {}".format(
											 label,
											 refValue,
											 outValue,
											 FabVal,
											 guiId,
											 id(fpItem)))

		self.myLogFile.writeline("mainPosdruckWindow", "updatePage", "-", self.myLogFile.DEF_INFO(), "ende")

	def __initGuiMainWin( self ):

		#########################
		# GUI
		#########################
		# Dies ist das Frame

		self.SetTitle(u"Positionsdruck ein- und aus-blenden")
		self.SetSize((740, 350))
		self.Centre()

		self.__initGuiMainWinMenuBar()
		self.__initGuiMainPanel()

	def __initGuiMainWinMenuBar( self ):

		# Menübarzeile erzeugen
		self.menubar = wx.MenuBar()
		self.fileMenu = wx.Menu()
		fileItem = self.fileMenu.Append(wx.ID_EXIT, "Quit", "Quit application")
		self.menubar.Append(self.fileMenu, "&File")
		self.SetMenuBar(self.menubar)
		self.Bind(wx.EVT_MENU, self.fnMenueQuit, fileItem)

	def __initGuiMainPanel_UpperPanel( self ):

		self.upperPanel.SetBackgroundColour('#dfdfdf')

		self.upperPanelHBox = wx.BoxSizer(wx.HORIZONTAL)
		self.upperPanel.SetSizer(self.upperPanelHBox)

		self.upperPanelSTxt1 = wx.StaticText(self.upperPanel, label = 'Bitte Auswahl treffen...')
		self.upperPanelSTxt1.SetFont(self.fontNormalText)
		self.upperPanelHBox.Add(self.upperPanelSTxt1)

		self.upperPanel.Layout()
		self.upperPanelHBox.Fit(self.upperPanel)

	def __initGuiMainPanel_MiddelPanel( self ):

		self.midPan.SetBackgroundColour('#6f6f6f')

		self.midPanVBox = wx.BoxSizer(wx.VERTICAL)
		self.midPan.SetSizer(self.midPanVBox)

		# self.testtext1 = wx.StaticText(self.midPan, label = 'Bauteile Selektieren..')

		# self.midPanVBox.Add(self.testtext1)

		self.midPanHBox = wx.BoxSizer(wx.HORIZONTAL)
		self.midPanVBox.Add(self.midPanHBox, 0, wx.EXPAND | wx.ALL, 5)

		self.MidPanCheckPanList = []
		self.MidPanMultiCheckboxList = []

		self.checkboxID = 0

		for checkPanNo in range(0, self.NumCheckboxColumns, 1):
			MidPanCheckPanEle = self.__initGuiMainPanel_MidPanCheckPan(self.NumCheckboxRows)
			self.MidPanCheckPanList.append(MidPanCheckPanEle)

		self.__initGuiMainPanel_MidPanButtonPan()

		self.midPan.Layout()
		self.midPanVBox.Fit(self.midPan)

	def __initGuiMainPanel_MidPanButtonPan( self ):

		VBoxOfColumn = wx.BoxSizer(wx.VERTICAL)
		self.midPanHBox.Add(VBoxOfColumn)
		
		GroupText = wx.StaticText(self.midPan, label = 'Alle Fab. Werte')
		self.ButtonAllFabValOn = wx.Button(self.midPan, wx.ID_ANY, u"Einblenden", wx.DefaultPosition, wx.DefaultSize, 0)
		self.ButtonAllFabValOff = wx.Button(self.midPan, wx.ID_ANY, u"Ausblenden", wx.DefaultPosition, wx.DefaultSize, 0)
		staticline = wx.StaticLine(self.midPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
		VBoxOfColumn.Add(GroupText, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.Add(self.ButtonAllFabValOn, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.Add(self.ButtonAllFabValOff, 5, wx.EXPAND | wx.ALL, 2)
		# VBoxOfColumn.AddSpacer((0, 5), 2, wx.EXPAND, 2)
		VBoxOfColumn.AddSpacer(5)
		VBoxOfColumn.Add(staticline, 1, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.AddSpacer(5)

		GroupText = wx.StaticText(self.midPan, label = 'Alle Referenzen')
		self.ButtonAllRefOn = wx.Button(self.midPan, wx.ID_ANY, u"Einblenden", wx.DefaultPosition, wx.DefaultSize, 0)
		self.ButtonAllRefOff = wx.Button(self.midPan, wx.ID_ANY, u"Ausblenden", wx.DefaultPosition, wx.DefaultSize, 0)
		staticline = wx.StaticLine(self.midPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
		VBoxOfColumn.Add(GroupText, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.Add(self.ButtonAllRefOn, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.Add(self.ButtonAllRefOff, 5, wx.EXPAND | wx.ALL, 2)
		# VBoxOfColumn.AddSpacer((0, 5), 2, wx.EXPAND, 2)
		VBoxOfColumn.AddSpacer(5)
		VBoxOfColumn.Add(staticline, 1, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.AddSpacer(5)

		staticline = wx.StaticLine(self.midPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
		self.midPanHBox.Add(staticline, 0, wx.EXPAND | wx.ALL, 1)
		VBoxOfColumn = wx.BoxSizer(wx.VERTICAL)
		self.midPanHBox.Add(VBoxOfColumn)

		GroupText = wx.StaticText(self.midPan, label = 'Konturen / Outline')
		self.ButtonAllOutlineOn = wx.Button(self.midPan, wx.ID_ANY, u"Einblenden", wx.DefaultPosition, wx.DefaultSize,
											0)
		self.ButtonAllOutlineOff = wx.Button(self.midPan, wx.ID_ANY, u"Ausblenden", wx.DefaultPosition, wx.DefaultSize,
											 0)
		staticline = wx.StaticLine(self.midPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
		VBoxOfColumn.Add(GroupText, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.Add(self.ButtonAllOutlineOn, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.Add(self.ButtonAllOutlineOff, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.AddSpacer(5)
		VBoxOfColumn.Add(staticline, 1, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.AddSpacer(5)


		GroupText = wx.StaticText(self.midPan, label = 'Alles...')
		self.ButtonAllOn = wx.Button(self.midPan, wx.ID_ANY, u"Einblenden", wx.DefaultPosition, wx.DefaultSize, 0)
		self.ButtonAllOff = wx.Button(self.midPan, wx.ID_ANY, u"Ausblenden", wx.DefaultPosition, wx.DefaultSize, 0)
		staticline = wx.StaticLine(self.midPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
		VBoxOfColumn.Add(GroupText, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.Add(self.ButtonAllOn, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.Add(self.ButtonAllOff, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.AddSpacer(5)
		VBoxOfColumn.Add(staticline, 1, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.AddSpacer(5)

		staticline = wx.StaticLine(self.midPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
		self.midPanHBox.Add(staticline, 0, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn = wx.BoxSizer(wx.VERTICAL)
		self.midPanHBox.Add(VBoxOfColumn)

		GroupText = wx.StaticText(self.midPan, label = 'Seitenwahl')
		self.ButtonNext = wx.Button(self.midPan, wx.ID_ANY, u"Vor", wx.DefaultPosition, wx.DefaultSize, 0)
		self.ButtonPrev = wx.Button(self.midPan, wx.ID_ANY, u"Zurück", wx.DefaultPosition, wx.DefaultSize, 0)
		staticline = wx.StaticLine(self.midPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
		self.StaticTextAktuelleSeite = wx.StaticText(self.midPan, label = '1 von 1')
		VBoxOfColumn.Add(GroupText, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.Add(self.ButtonNext, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.Add(self.ButtonPrev, 5, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.AddSpacer(5)
		VBoxOfColumn.Add(self.StaticTextAktuelleSeite, 10, wx.EXPAND | wx.ALL, 2)
		VBoxOfColumn.AddSpacer(5)
		VBoxOfColumn.Add(staticline, 1, wx.ALL | wx.ALIGN_CENTER, 2)

		self.__ButtonBindingsButtonPan()

	def __initGuiMainPanel_MidPanCheckPan( self, AddNoitem ):

		VBoxOfColumn = wx.BoxSizer(wx.VERTICAL)
		self.midPanHBox.Add(VBoxOfColumn)
		staticline = wx.StaticLine(self.midPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
		self.midPanHBox.Add(staticline, 0, wx.EXPAND | wx.ALL, 2)

		# topline

		hboxTopline = wx.BoxSizer(wx.HORIZONTAL)
		VBoxOfColumn.Add(hboxTopline)
		toplineTextReference = wx.StaticText(self.midPan, label = 'Ref.')
		toplineTextOutline = wx.StaticText(self.midPan, label = 'Outl.')
		toplineTextFabVal = wx.StaticText(self.midPan, label = 'FabVal.')
		toplineTextBauteilText = wx.StaticText(self.midPan, label = 'Bauteil')
		hboxTopline.Add(toplineTextReference, 2, wx.EXPAND | wx.ALL, 2)
		hboxTopline.Add(toplineTextOutline, 2, wx.EXPAND | wx.ALL, 2)
		hboxTopline.Add(toplineTextFabVal, 2, wx.EXPAND | wx.ALL, 2)
		hboxTopline.Add(toplineTextBauteilText, 4, wx.EXPAND | wx.ALL, 2)

		staticline = wx.StaticLine(self.midPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
		VBoxOfColumn.Add(staticline, 0, wx.EXPAND | wx.ALL, 0)

		# Add Checkboxen

		for count in range(0, AddNoitem, 1):
			checkboxRowSizer = self.__init_CheckboxRow()
			VBoxOfColumn.Add(checkboxRowSizer)
			staticline = wx.StaticLine(self.midPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
			VBoxOfColumn.Add(staticline, 0, wx.EXPAND | wx.ALL, 0)

	def __init_CheckboxRow( self ):

		checkboxRowSizer = wx.BoxSizer(wx.HORIZONTAL)

		cheboxItem = multiCheckboxRow(self.myLogFile, self.midPan, self.checkboxID)
		self.checkboxID += 1

		checkboxRowSizer.Add(cheboxItem.getWxCheckboxRefObj(), 2, wx.EXPAND | wx.ALL, 2)
		checkboxRowSizer.Add(cheboxItem.getWxChekboxOutlineObj(), 2, wx.EXPAND | wx.ALL, 2)
		checkboxRowSizer.Add(cheboxItem.getWxChekboxFabValObj(), 2, wx.EXPAND | wx.ALL, 2)
		checkboxRowSizer.Add(cheboxItem.getWxTextObj(), 4, wx.EXPAND | wx.ALL, 2)

		self.MidPanMultiCheckboxList.append(cheboxItem)

		return checkboxRowSizer

	def __initGuiMainPanel_LowerPanel( self ):

		self.lowerPanel.SetBackgroundColour('#9f9f9f')

		self.lowerPanHBox = wx.BoxSizer(wx.HORIZONTAL)
		self.lowerPanel.SetSizer(self.lowerPanHBox)

		self.lowerPanHBox.AddSpacer(100)
		self.lowerPanButtonOK = wx.Button(self.lowerPanel, wx.ID_ANY, u"OK", wx.DefaultPosition, wx.DefaultSize, 0)
		self.lowerPanHBox.Add(self.lowerPanButtonOK, 1, wx.ALIGN_CENTER | wx.ALL, 0)
		self.lowerPanHBox.AddSpacer(25)
		self.lowerPanButtonCancel = wx.Button(self.lowerPanel, wx.ID_ANY, u"CANCEL", wx.DefaultPosition, wx.DefaultSize,
											  0)
		self.lowerPanHBox.Add(self.lowerPanButtonCancel, 1, wx.ALIGN_CENTER | wx.ALL, 0)
		self.lowerPanHBox.AddSpacer(100)

		self.__ButtonBindingsLowPan()

	def __initGuiMainPanel( self ):

		self.fontNormalText = wx.SystemSettings.GetFont(wx.SYS_SYSTEM_FONT)

		self.fontNormalText.SetPointSize(9)

		self.MainPanel = wx.Panel(self)

		self.MainPanel.SetBackgroundColour('#000000')
		self.MainPanelVBox = wx.BoxSizer(wx.VERTICAL)

		self.upperPanel = wx.Panel(self.MainPanel)
		self.__initGuiMainPanel_UpperPanel()
		self.MainPanelVBox.Add(self.upperPanel, 1, wx.EXPAND | wx.ALL, 0)

		self.midPan = wx.Panel(self.MainPanel)
		self.__initGuiMainPanel_MiddelPanel()
		self.MainPanelVBox.Add(self.midPan, 20, wx.EXPAND | wx.ALL, 0)

		self.lowerPanel = wx.Panel(self.MainPanel)
		self.__initGuiMainPanel_LowerPanel()
		self.MainPanelVBox.Add(self.lowerPanel, 1, wx.EXPAND | wx.ALL, 0)

		self.MainPanel.SetSizer(self.MainPanelVBox)

	def __ButtonBindingsLowPan( self ):

		self.lowerPanButtonOK.Bind(wx.EVT_LEFT_UP, self.fnButtonOk)
		self.lowerPanButtonCancel.Bind(wx.EVT_LEFT_UP, self.fnButtonCanel)

	def __ButtonBindingsButtonPan( self ):

		self.ButtonAllFabValOn.Bind(wx.EVT_LEFT_UP, self.fnButtonAlleFabValOn)
		self.ButtonAllFabValOff.Bind(wx.EVT_LEFT_UP, self.fnButtonAlleFabValOff)
		
		self.ButtonAllRefOn.Bind(wx.EVT_LEFT_UP, self.fnButtonAlleRefOn)
		self.ButtonAllRefOff.Bind(wx.EVT_LEFT_UP, self.fnButtonAlleRefOff)

		self.ButtonAllOutlineOn.Bind(wx.EVT_LEFT_UP, self.fnButtonAlleOutlineOn)
		self.ButtonAllOutlineOff.Bind(wx.EVT_LEFT_UP, self.fnButtonAlleOutlineOff)

		self.ButtonAllOn.Bind(wx.EVT_LEFT_UP, self.fnButtonAllesOn)
		self.ButtonAllOff.Bind(wx.EVT_LEFT_UP, self.fnButtonAllesOff)

		self.ButtonNext.Bind(wx.EVT_LEFT_UP, self.fnButtonNextPage)
		self.ButtonPrev.Bind(wx.EVT_LEFT_UP, self.fnButtonPrevPage)

	def fnMenueQuit( self, event ):

		self.myLogFile.writeline("mainPosdruckWindow", "fnMenueQuit", "-", self.myLogFile.DEF_INFO(), "ENDE")

		event.Skip()
		self.Close()

	def fnButtonOk( self, event ):

		self.updateTextTopline(" Positionsdruck wird übernommen. Bitte warten...")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonOk", "-", self.myLogFile.DEF_INFO(), "Start")
		
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonOk", "-", self.myLogFile.DEF_INFO(), "storeCheckboxValue")
		self.storeCheckboxValue()
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonOk", "-", self.myLogFile.DEF_INFO(), "storeCheckboxValue...Done")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonOk", "-", self.myLogFile.DEF_INFO(), "RunModifyFootprints")
		self.Posdruck.RunModifyFootprints()
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonOk", "-", self.myLogFile.DEF_INFO(), "RunModifyFootprints...Done")

		self.updateTextTopline("Positionsdruck  übernommen.")
		Refresh()
		event.Skip()
		self.Close()

	def fnButtonCanel( self, event ):

		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonCancel", "-", self.myLogFile.DEF_INFO(), "Start")

		self.updateTextTopline("Abbruch.")
		self.Close()
		event.Skip()

	def fnButtonAlleFabValOn( self, event ):

		self.updateTextTopline("Alle Fab. Werte einblenden")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonAlleFabValOn", "-", self.myLogFile.DEF_INFO(), "Start")

		for fpItem in self.FPonVisPageList:
			if (isinstance(fpItem, FootprintMkk)):
				fpItem.setFootprintFabValVisibility(True)

		# self.storeCheckboxValue() # Nicht rufen, da der Anzeigezustand Egal ist und aktualisiert werden muss
		self.updatePage()
		self.ToplineStdText()
		event.Skip()

	def fnButtonAlleFabValOff( self, event ):

		starttime = time.time()

		self.updateTextTopline("Alle Fab. Werte  ausblenden")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonAlleFabValOff", "-", self.myLogFile.DEF_INFO(), "Start")

		afterlogtime = time.time()

		for fpItem in self.FPonVisPageList:
			if (isinstance(fpItem, FootprintMkk)):
				fpItem.setFootprintFabValVisibility(False)

		afterBtTime = time.time()

		# self.storeCheckboxValue() # Nicht rufen, da der Anzeigezustand Egal ist und aktualisiert werden muss
		self.updatePage()
		self.ToplineStdText()
		event.Skip()

	def fnButtonAlleRefOn( self, event ):

		self.updateTextTopline("Alle Bauteilrefernzen einblenden")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonAlleRefOn", "-", self.myLogFile.DEF_INFO(), "Start")

		for fpItem in self.FPonVisPageList:
			if (isinstance(fpItem, FootprintMkk)):
				fpItem.setFootprintReferneceVisibility(True)

		# self.storeCheckboxValue() # Nicht rufen, da der Anzeigezustand Egal ist und aktualisiert werden muss
		self.updatePage()
		self.ToplineStdText()
		event.Skip()

	def fnButtonAlleRefOff( self, event ):

		starttime = time.time()

		self.updateTextTopline("Alle Bauteilrefernzen ausblenden")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonAlleRefOff", "-", self.myLogFile.DEF_INFO(), "Start")

		afterlogtime = time.time()

		for fpItem in self.FPonVisPageList:
			if (isinstance(fpItem, FootprintMkk)):
				fpItem.setFootprintReferneceVisibility(False)

		afterBtTime = time.time()

		# self.storeCheckboxValue() # Nicht rufen, da der Anzeigezustand Egal ist und aktualisiert werden muss
		self.updatePage()
		#self.storeCheckboxValue()

		afterPageTime = time.time()

		totalTime = afterPageTime-starttime
		logtime = afterlogtime-starttime
		BtTime = afterBtTime-afterlogtime
		PageTime = afterPageTime-afterBtTime

		self.ToplineStdText()
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonAlleRefOff", "-", self.myLogFile.DEF_INFO(),
								 "TimingInfo : total {} ; log {} ; BtUp {} ; PageUp {} ; ".format(totalTime, logtime,
																								  BtTime, PageTime))
		event.Skip()

	def fnButtonAlleOutlineOn( self, event ):

		self.updateTextTopline("Alle Bauteilkonturen einblenden")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonAlleOutlineOn", "-", self.myLogFile.DEF_INFO(), "Start")

		for fpItem in self.FPonVisPageList:
			if (isinstance(fpItem, FootprintMkk)):
				fpItem.setFootprintOutlineVisibility(True)

		# self.storeCheckboxValue() # Nicht rufen, da der Anzeigezustand Egal ist und aktualisiert werden muss
		self.updatePage()
		self.ToplineStdText()
		event.Skip()

	def fnButtonAlleOutlineOff( self, event ):

		self.updateTextTopline("Alle Bauteilkonturen ausblenden")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonAlleOutlineOff", "-", self.myLogFile.DEF_INFO(),
								 "Start")

		for fpItem in self.FPonVisPageList:
			if (isinstance(fpItem, FootprintMkk)):
				fpItem.setFootprintOutlineVisibility(False)

		# self.storeCheckboxValue() # Nicht rufen, da der Anzeigezustand Egal ist und aktualisiert werden muss
		self.updatePage()
		self.ToplineStdText()
		event.Skip()

	def fnButtonAllesOn(self, event):

		self.updateTextTopline("Alles einblenden")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonAllesOn", "-", self.myLogFile.DEF_INFO(), "Start")

		for fpItem in self.Posdruck.getFPList():
			if (isinstance(fpItem, FootprintMkk)):
				fpItem.setFootprintOutlineVisibility(True)
				fpItem.setFootprintReferneceVisibility(True)
				fpItem.setFootprintFabValVisibility(True)

		self.updatePage()
		self.ToplineStdText()
		event.Skip()

	def fnButtonAllesOff(self, event):

		self.updateTextTopline("Alles ausblenden")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonAllesOff", "-", self.myLogFile.DEF_INFO(), "Start")

		for fpItem in self.Posdruck.getFPList():
			if (isinstance(fpItem, FootprintMkk)):
				fpItem.setFootprintOutlineVisibility(False)
				fpItem.setFootprintReferneceVisibility(False)
				fpItem.setFootprintFabValVisibility(False)

		self.updatePage()
		self.ToplineStdText()
		event.Skip()

	def fnButtonNextPage( self, event ):

		self.updateTextTopline("Nachste Seite laden...")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonNextPage", "-", self.myLogFile.DEF_INFO(), "NextPage")

		nextpage = self.AktuelleSeite+1

		if (nextpage <= self.AnzahlSeiten):
			self.myLogFile.writeline("mainPosdruckWindow", "fnButtonNextPage", "-", self.myLogFile.DEF_INFO(),
									 "switch to Page {}".format(nextpage))
			self.storeCheckboxValue()
			self.switchPage(nextpage)
		self.ToplineStdText()
		event.Skip()

	def fnButtonPrevPage( self, event ):

		self.updateTextTopline("Vorrige Seite laden...")
		self.myLogFile.writeline("mainPosdruckWindow", "fnButtonPrevPage", "-", self.myLogFile.DEF_INFO(), "PrevPage")

		nextpage = self.AktuelleSeite-1

		if (nextpage >= 1):
			self.myLogFile.writeline("mainPosdruckWindow", "fnButtonPrevPage", "-", self.myLogFile.DEF_INFO(),
									 "switch to Page {}".format(nextpage))
			self.storeCheckboxValue()
			self.switchPage(nextpage)

		self.ToplineStdText()
		event.Skip()


class Positionsdruck(ActionPlugin):

	def __init__( self ):
		super(Positionsdruck, self).__init__()

		########################
		# Allgemeines und logging
		########################

		self.myLogFile = mylogger()


		self.myLogFile.startNewFile()

		self.myLogFile.writeline("Positionsdruck", "__init__", "-", self.myLogFile.DEF_INFO(), "start")

		self.mainApp = None  # self.mainApp = wx.App()
		self.mainWindow = None  # self.mainWindow = mainPosdruckWindow()

		self.myLogFile.writeline("Positionsdruck", "__init__", "-", self.myLogFile.DEF_INFO(), "done")

	def defaults( self ):
		self.name = "Positionsdruck"
		self.category = " Modifiezieren"
		self.description = " Ein- und Ausblenden von Positionsdruck einzelnder Bauteile"
		icon_dir = os.path.dirname(os.path.dirname(__file__))
		self.icon_file_name = os.path.join(icon_dir, 'icon.png')
		self.pcbnew_icon_support = hasattr(self, "show_toolbar_button")
		self.show_toolbar_button = True

	def Run( self ):
		# print("Running Positoinsdruck")

		self.myLogFile.writeline("Positionsdruck", "Run", "-", self.myLogFile.DEF_INFO(), "start")
		self.mainApp = None
		while (self.mainApp == None):
			self.mainApp = wx.App()  # clearSigInt = True
			time.sleep(0.2)

		self.myLogFile.writeline("Positionsdruck", "VarLog", "-", self.myLogFile.DEF_INFO(), "icon_file_name {}".format(self.icon_file_name))
		self.myLogFile.writeline("Positionsdruck", "VarLog", "-", self.myLogFile.DEF_INFO(), "pcbnew_icon_support {}".format(self.pcbnew_icon_support))

		self.mainWindow = mainPosdruckWindow(self.myLogFile, None)
		self.mainWindow.Show()


if __name__ == '__main__':

	print("Dies ist ein Plugin, dass aus KiCad gestartet werden muss.")





